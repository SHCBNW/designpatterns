package command_pattern;

public class Client {
    public static void main(String[] args) {
        Invoker boss = new InvokerBoss();
        Invoker officeguy = new InvokerOfficeGuy();

        ReceiverPrinter entrance = new ReceiverPrinterEntrance();
        ReceiverPrinter office = new ReceiverPrinterOffice();

        String document1 = "You are fired.";
        String document2 = "You are fired as well.";

        boss.setPrintCommand(new CommandPrintBW(entrance));
        boss.performPrintCommand(document1);
        boss.setPrintCommand(new CommandPrintBW(office));
        boss.performPrintCommand(document2);
    }
}
