package command_pattern;

public interface ReceiverPrinter {

    void config(String mode);

    void print(String document);
}
