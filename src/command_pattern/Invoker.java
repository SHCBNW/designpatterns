package command_pattern;

public interface Invoker {
    void setPrintCommand(Command command);

    void performPrintCommand(String document);
}
