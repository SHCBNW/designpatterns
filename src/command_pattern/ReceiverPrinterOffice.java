package command_pattern;

public class ReceiverPrinterOffice implements ReceiverPrinter {
    String mode = "color";

    @Override
    public void config(String mode) {
        this.mode = mode;
    }

    @Override
    public void print(String document) {
        System.out.println("PRINTING: " + document);
    }
}
