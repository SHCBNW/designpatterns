package command_pattern;

public class CommandPrintColor implements Command {
    ReceiverPrinter printer;

    public CommandPrintColor(ReceiverPrinter printer) {
        this.printer = printer;
    }

    @Override
    public void executePrint(String document) {
        printer.config("oolor");
        printer.print(document);
    }
}
