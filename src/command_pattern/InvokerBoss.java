package command_pattern;

public class InvokerBoss implements Invoker {
    Command command;

    @Override
    public void setPrintCommand(Command command) {
        this.command = command;
    }

    @Override
    public void performPrintCommand(String document) {
        this.command.executePrint(document);
    }
}
