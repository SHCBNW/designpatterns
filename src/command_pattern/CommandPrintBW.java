package command_pattern;

public class CommandPrintBW implements Command {
    ReceiverPrinter printer;

    public CommandPrintBW(ReceiverPrinter printer) {
        this.printer = printer;
    }

    @Override
    public void executePrint(String document) {
        printer.config("bw");
        printer.print(document);
    }
}
