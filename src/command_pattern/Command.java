package command_pattern;

public interface Command {
    public void executePrint(String document);
}
