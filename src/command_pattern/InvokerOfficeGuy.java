package command_pattern;

public class InvokerOfficeGuy implements Invoker {
    private Command command;

    @Override
    public void setPrintCommand(Command command) {
        this.command = command;
    }

    @Override
    public void performPrintCommand(String document) {
        this.command.executePrint(document);
    }
}
