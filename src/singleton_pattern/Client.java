package singleton_pattern;

public class Client {
    public static void main(String[] args) {
        SingletonEager s1 = SingletonEager.getInstance();
        SingletonEager s2 = SingletonEager.getInstance();
        System.out.println(s1);
        System.out.println(s2);
        SingletonLazy s3 = SingletonLazy.getInstance();
        SingletonLazy s4 = SingletonLazy.getInstance();
        System.out.println(s3);
        System.out.println(s4);
    }
}
