package observer_pattern;

import java.util.ArrayList;
import java.util.List;

public class SubjectWeatherstation implements Subject {

    List<Observer> list = new ArrayList<>();
    double temperature = 20.0;

    // Getter and setter
    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    // Observer stuff

    @Override
    public void addObserver(Observer b) {
        list.add(b);
    }

    @Override
    public void removeObserver(Observer b) {
        list.remove(b);
    }

    @Override
    public void notifyAllObserver() {
        for (Observer observer : list) observer.update();
    }
}
