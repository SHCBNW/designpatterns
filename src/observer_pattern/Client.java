package observer_pattern;

public class Client {
    public static void main(String[] args) {
        SubjectWeatherstation wetterstation = new SubjectWeatherstation();

        Observer pc = new ObserverPC(wetterstation);
        Observer tv = new ObserverTV(wetterstation);
        Observer uhr = new ObserverClock(wetterstation);

        wetterstation.addObserver(pc);
        wetterstation.addObserver(tv);
        wetterstation.addObserver(uhr);

        wetterstation.setTemperature(40.2);

        wetterstation.notifyAllObserver();

        pc.print();
        tv.print();
        uhr.print();

        wetterstation.removeObserver(uhr);
        wetterstation.setTemperature(20.4);
        wetterstation.notifyAllObserver();

        pc.print();
        tv.print();
        uhr.print();
    }
}
