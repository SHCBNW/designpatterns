package observer_pattern;

public class ObserverPC implements Observer {

    private Subject subject;

    private double temperature;

    ObserverPC(Subject subject) {
        this.subject = subject;
    }

    private double getTemperature() {
        return temperature;
    }

    private void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    @Override
    public void update() {
        setTemperature(((SubjectWeatherstation) subject).getTemperature());
    }

    @Override
    public void print() {
        System.out.println(getTemperature());
    }
}
