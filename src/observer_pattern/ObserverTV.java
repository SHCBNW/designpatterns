package observer_pattern;

public class ObserverTV implements Observer {


    private Subject subject;

    private double temperatur;

    ObserverTV(Subject subject) {
        this.subject = subject;
    }

    private double getTemperatur() {
        return temperatur;
    }

    public void setTemperatur(double temperatur) {
        this.temperatur = temperatur;
    }

    @Override
    public void update() {
        this.temperatur = ((SubjectWeatherstation) subject).getTemperature();
    }

    @Override
    public void print() {
        System.out.println(getTemperatur());
    }
}
