package observer_pattern;


public class ObserverClock implements Observer {


    private Subject subject;

    private double temperature;

    ObserverClock(Subject subject) {
        this.subject = subject;
    }

    private double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    @Override
    public void update() {
        this.temperature = ((SubjectWeatherstation) subject).getTemperature();
    }

    @Override
    public void print() {
        System.out.println(getTemperature());
    }


}
