package observer_pattern;

public interface Observer {

    public void update();

    public void print();

}
