package observer_pattern;

public interface Subject {

    public void addObserver(Observer b);

    public void removeObserver(Observer b);

    public void notifyAllObserver();
}
