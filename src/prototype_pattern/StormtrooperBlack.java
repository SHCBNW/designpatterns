package prototype_pattern;

public class StormtrooperBlack extends Stormtrooper {
    public StormtrooperBlack(int id, String name) {
        super.id = id;
        super.name = name;
    }

    @Override
    void print() {
        System.out.println("I am a Blacktrooper and my name is " + super.getName() + " (" + super.getId() + ")");
    }
}
