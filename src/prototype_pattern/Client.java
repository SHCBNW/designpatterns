package prototype_pattern;

public class Client {
    public static void main(String[] args) {
        Stormtrooper siu = new StormtrooperBlack(1, "Siu");
        Stormtrooper til = new StormtrooperChrome(2, "Til");
        Stormtrooper david = new StormtrooperWhite(3, "David");
        siu.print();
        til.print();
        david.print();

        Stormtrooper siuClone = (Stormtrooper) siu.clone();
        siuClone.print();

        System.out.println(siu);
        System.out.println(siuClone);

        siuClone.setName("Siu Ho");
        siuClone.setId(4);
        siuClone.print();
        siu.print();

    }
}
