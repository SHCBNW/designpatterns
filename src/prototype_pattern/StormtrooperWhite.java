package prototype_pattern;

public class StormtrooperWhite extends Stormtrooper {
    public StormtrooperWhite(int id, String name) {
        super.id = id;
        super.name = name;
    }

    @Override
    void print() {
        System.out.println("I am a Whitetrooper and my name is " + super.getName() + " (" + super.getId() + ")");
    }
}
