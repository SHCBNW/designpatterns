package prototype_pattern;

public class StormtrooperChrome extends Stormtrooper {
    public StormtrooperChrome(int id, String name) {
        super.id = id;
        super.name = name;
    }
    @Override
    void print() {
        System.out.println("I am a Chrometrooper and my name is " + super.getName() + " (" + super.getId() + ")");
    }
}
