package template_method_pattern;

public abstract class CookInstantNoodles {

    final void cook() {
        boilWater();
        ramenNoodles();
        getThatSauce();
        spiceItUp();
    }

    protected abstract void getThatSauce();

    protected abstract void ramenNoodles();

    private void boilWater() {
        System.out.println("Cold water, boil it up, get get get it hot");
    }

    private void spiceItUp() {
        System.out.println("Sriracha Sauce, Sriracha Sauce");
    }
}
