package template_method_pattern;

public class TopRamen extends CookInstantNoodles {

    @Override
    protected void ramenNoodles() {
        System.out.println("Order noodle, put it in, stir stir stir it up");
    }

    @Override
    protected void getThatSauce() {
        System.out.println("Hold that, get that sauce, spice it up");
    }
}
