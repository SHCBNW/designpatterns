package builder_pattern;

public abstract class PersonAbstract {

    abstract PersonAbstract setFirstName(String s);

    abstract PersonAbstract setLastName(String s);

    abstract PersonAbstract setSex(String s);

    abstract PersonAbstract setNationality(String s);

    abstract PersonAbstract print();

}
