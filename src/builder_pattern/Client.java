package builder_pattern;

public class Client {

    public static void main(String[] args) {
        PersonAbstract siu = new Person();
        siu.setFirstName("Siu").setLastName("Cheng").setSex("M").setNationality("German").print();
    }

}
