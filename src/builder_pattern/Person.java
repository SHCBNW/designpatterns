package builder_pattern;

public class Person extends PersonAbstract {
    String firstName;
    String lastName;
    String sex;
    String nationality;

    protected Person() {
    }

    @Override
    PersonAbstract setFirstName(String s) {
        this.firstName = s;
        return this;
    }

    @Override
    PersonAbstract setLastName(String s) {
        this.lastName = s;
        return this;
    }

    @Override
    PersonAbstract setSex(String s) {
        this.sex = s;
        return this;
    }

    @Override
    PersonAbstract setNationality(String s) {
        this.nationality = s;
        return this;
    }

    @Override
    PersonAbstract print() {
        System.out.println(this.firstName + " " + this.lastName + ", " + this.sex + ", " + this.nationality);
        return this;
    }
}
