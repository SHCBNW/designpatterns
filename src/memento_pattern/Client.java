package memento_pattern;

public class Client {
    public static void main(String[] args) {
        CareTaker careTaker = new CareTaker();
        Document doc = new Document();
        doc.setText("First Text");
        doc.print();
        careTaker.add(doc.saveStateToMemento());
        doc.setText("Second Text");
        doc.print();
        doc.getStateFromMemento(careTaker.getLast());
        doc.print();
    }
}
