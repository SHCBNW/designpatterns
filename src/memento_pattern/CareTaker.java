package memento_pattern;

import java.util.ArrayList;
import java.util.List;

public class CareTaker {
    private List<Memento> list = new ArrayList<>();

    public void add(Memento memento) {
        list.add(memento);
    }

    public Memento get(int index) {
        return list.get(index);
    }

    public Memento getLast() {
        return get(list.size() - 1);
    }

    public void removeLast() {
        list.remove(list.size() - 1);
    }
}
