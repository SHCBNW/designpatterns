package memento_pattern;

public class Document {
    private String text;

    public void setText(String state) {
        this.text = state;
    }

    public String getText() {
        return text;
    }

    public Memento saveStateToMemento() {
        return new Memento(text);
    }

    public void getStateFromMemento(Memento memento) {
        text = memento.getState();
    }

    public void print() {
        System.out.println(getText());
    }
}
