package state_pattern;

public interface State {
    void next(Package pkg);

    void prev(Package pkg);

    void print();
}
