package state_pattern;

public class StateB implements State {

    @Override
    public void next(Package pkg) {
        pkg.setState(new StateC());
    }

    @Override
    public void prev(Package pkg) {
        pkg.setState(new StateA());
    }

    @Override
    public void print() {
        System.out.println("State is B");
    }
}
