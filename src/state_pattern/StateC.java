package state_pattern;

public class StateC implements State {

    @Override
    public void next(Package pkg) {
        pkg.setState(new StateA());
    }

    @Override
    public void prev(Package pkg) {
        pkg.setState(new StateB());
    }

    @Override
    public void print() {
        System.out.println("State is C");
    }
}
