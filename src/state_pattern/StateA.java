package state_pattern;

public class StateA implements State {

    @Override
    public void next(Package pkg) {
        pkg.setState(new StateB());
    }

    @Override
    public void prev(Package pkg) {
        pkg.setState(new StateC());
    }

    @Override
    public void print() {
        System.out.println("State is A");
    }
}
