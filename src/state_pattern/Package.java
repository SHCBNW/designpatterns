package state_pattern;

public class Package {

    private State state = new StateA();

    public void setState(State state) {
        this.state = state;
    }

    public void previousState() {
        state.prev(this);
    }

    public void nextState() {
        state.next(this);
    }

    public void printStatus() {
        state.print();
    }
}
