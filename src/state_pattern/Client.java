package state_pattern;

public class Client {
    public static void main(String[] args) {
        Package p = new Package();
        p.printStatus();

        p.nextState();
        p.printStatus();

        p.nextState();
        p.printStatus();

        p.nextState();
        p.printStatus();

        p.previousState();
        p.printStatus();
    }
}
