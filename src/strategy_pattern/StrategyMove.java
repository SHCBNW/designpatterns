package strategy_pattern;

public interface StrategyMove {
    void move();
}
