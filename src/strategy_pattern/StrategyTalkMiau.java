package strategy_pattern;

public class StrategyTalkMiau implements StrategyTalk {
    @Override
    public void talk() {
        System.out.println("Miaaau!!");
    }
}
