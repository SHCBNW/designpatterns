package strategy_pattern;

public class StrategyTalkQuak implements StrategyTalk {
    @Override
    public void talk() {
        System.out.println("Quak!!");
    }
}
