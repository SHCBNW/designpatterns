package strategy_pattern;

public class StrategyMoveFly implements StrategyMove {
    @Override
    public void move() {
        System.out.println("I an flying!");
    }
}
