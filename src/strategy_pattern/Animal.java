package strategy_pattern;

public abstract class Animal {

    StrategyMove move;
    StrategyTalk talk;


    public void setStrategyMove(StrategyMove move) {
        this.move = move;
    }

    public void setStrategyTalk(StrategyTalk talk) {
        this.talk = talk;
    }

    public void move() {
        move.move();
    }

    public void talk() {
        talk.talk();
    }
}
