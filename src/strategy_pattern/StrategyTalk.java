package strategy_pattern;

public interface StrategyTalk {
    void talk();
}
