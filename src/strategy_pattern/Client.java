package strategy_pattern;

public class Client {

    public static void main(String[] args) {
        Animal cat = new AnimalCat();
        Animal dog = new AnimalDog();
        Animal duck = new AnimalDuck();
        cat.move();
        cat.talk();
        dog.move();
        dog.talk();
        duck.move();
        duck.talk();
    }

}
