package strategy_pattern;

public class StrategyMoveRun implements StrategyMove {
    @Override
    public void move() {
        System.out.println("I am running!");
    }
}
