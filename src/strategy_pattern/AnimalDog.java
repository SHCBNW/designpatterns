package strategy_pattern;

class AnimalDog extends Animal {

    AnimalDog() {
        setStrategyMove(new StrategyMoveRun());
        setStrategyTalk(new StrategyTalkWau());
    }
}
