package strategy_pattern;

class AnimalDuck extends Animal {

    AnimalDuck() {
        setStrategyMove(new StrategyMoveFly());
        setStrategyTalk(new StrategyTalkQuak());
    }
}
