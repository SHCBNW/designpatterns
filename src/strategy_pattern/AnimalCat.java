package strategy_pattern;

class AnimalCat extends Animal {

    AnimalCat() {
        setStrategyMove(new StrategyMoveRun());
        setStrategyTalk(new StrategyTalkMiau());
    }
}
