package strategy_pattern;

public class StrategyTalkWau implements StrategyTalk {
    @Override
    public void talk() {
        System.out.println("Wau Wau!!");
    }
}
