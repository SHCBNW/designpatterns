package facade_pattern;

public class Client {
    public static void main(String[] args) {
        Facade pc = new FacadePC();
        Facade ps = new FacadePlaystation();

        pc.start();
        ps.start();

        pc.end();
        ps.end();
    }
}
