package facade_pattern;

public class DevicePC implements Device {
    @Override
    public void turnOn() {
        System.out.println("Turning on PC");
    }

    @Override
    public void turnOff() {
        System.out.println("Turning off PC");
    }}
