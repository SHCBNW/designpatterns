package facade_pattern;

public interface Facade {
    public void start();

    public void end();
}
