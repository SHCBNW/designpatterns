package facade_pattern;

public class FacadePC implements Facade {
    DevicePC pc = new DevicePC();
    DeviceDisplay display = new DeviceDisplay();
    DeviceSpeaker speaker = new DeviceSpeaker();

    @Override
    public void start() {
        pc.turnOn();
        display.turnOn();
        speaker.turnOn();
    }

    @Override
    public void end() {
        pc.turnOff();
        display.turnOff();
        speaker.turnOff();
    }
}
