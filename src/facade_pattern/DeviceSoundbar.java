package facade_pattern;

public class DeviceSoundbar implements Device {
    @Override
    public void turnOn() {
        System.out.println("Turning on Soundbar");
    }

    @Override
    public void turnOff() {
        System.out.println("Turning off Soundbar");
    }
}
