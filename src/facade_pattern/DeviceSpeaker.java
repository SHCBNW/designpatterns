package facade_pattern;

public class DeviceSpeaker implements Device {
    @Override
    public void turnOn() {
        System.out.println("Turning on Speaker");
    }

    @Override
    public void turnOff() {
        System.out.println("Turning off Speaker");
    }
}
