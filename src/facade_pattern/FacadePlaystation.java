package facade_pattern;

public class FacadePlaystation implements Facade {
    DeviceTV tv = new DeviceTV();
    DeviceSoundbar soundBar = new DeviceSoundbar();
    DevicePlaystation playstation = new DevicePlaystation();

    @Override
    public void start() {
        tv.turnOn();
        soundBar.turnOn();
        playstation.turnOn();
    }

    @Override
    public void end() {
        tv.turnOff();
        soundBar.turnOff();
        playstation.turnOff();
    }
}
