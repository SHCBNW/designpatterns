package facade_pattern;

public class DevicePlaystation implements Device {
    @Override
    public void turnOn() {
        System.out.println("Turning on Playstation");
    }

    @Override
    public void turnOff() {
        System.out.println("Turning off Playstation");
    }
}
