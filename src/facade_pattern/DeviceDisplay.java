package facade_pattern;

public class DeviceDisplay implements Device {
    @Override
    public void turnOn() {
        System.out.println("Turning on Display");
    }

    @Override
    public void turnOff() {
        System.out.println("Turning off Display");
    }
}
