package decorator_pattern;

public interface Order {
    double getPrice();

    void printOrder();
}
