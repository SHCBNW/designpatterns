package decorator_pattern;

public class SideDisheEgg extends SideDishe {

    public SideDisheEgg(Order component) {
        super(component);
    }

    @Override
    public double getPrice() {
        return super.component.getPrice() + 0.55;
    }

    @Override
    public void printOrder() {
        super.component.printOrder();
        System.out.print(", Egg");
    }
}
