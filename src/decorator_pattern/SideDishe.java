package decorator_pattern;

public abstract class SideDishe implements Order {
    protected Order component;

    public SideDishe(Order component) {
        this.component = component;
    }
}
