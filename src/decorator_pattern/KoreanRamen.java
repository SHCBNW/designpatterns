package decorator_pattern;

public class KoreanRamen implements Order {

    @Override
    public double getPrice() {
        return 4.49;
    }

    @Override
    public void printOrder() {
        System.out.print("Korean Ramen");
    }
}
