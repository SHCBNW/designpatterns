package decorator_pattern;

public class SideDisheSriracha extends SideDishe {

    public SideDisheSriracha(Order component) {
        super(component);
    }

    @Override
    public double getPrice() {
        return super.component.getPrice() + 0.48;
    }

    @Override
    public void printOrder() {
        super.component.printOrder();
        System.out.print(", Sriracha");
    }
}
