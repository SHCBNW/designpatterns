package decorator_pattern;

public class Client {
    public static void main(String[] args) {
        Order r1 = new SideDisheEgg(new SideDisheSriracha(new JapaneseRamen()));
        Order r2 = new SideDisheEgg(new KoreanRamen());
        r1.printOrder();
        System.out.print(" for " + r1.getPrice()+"\n");
        r2.printOrder();
        System.out.print(" for " + r2.getPrice()+"\n");

    }
}
