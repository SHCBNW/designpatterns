package decorator_pattern;

public class JapaneseRamen implements Order {

    @Override
    public double getPrice() {
        return 5.99;
    }

    @Override
    public void printOrder() {
        System.out.print("Japanese Ramen");
    }
}
