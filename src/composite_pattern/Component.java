package composite_pattern;

public abstract class Component {
    public abstract void operation();

    public abstract String getName();

    public abstract void setName(String name);

    public void add(Component comp) {
        // todo
    }

    public void remove(Component comp) {
        // todo
    }

    public Component getChild(int index) {
        // todo
        return null;
    }

}