package composite_pattern;

import java.util.ArrayList;
import java.util.List;

public class Client {
    public static void main(String[] args) {
        List<Component> list = new ArrayList<>();

        Component leiter1 = new Composite("Siu");
        Component leiter2 = new Composite("David");
        Component leiter3 = new Composite("Til");
        list.add(leiter1);
        list.add(leiter2);
        list.add(leiter3);

        leiter1.add(leiter2);
        leiter1.add(leiter3);

        Component arbeiter1 = new Leaf("Alex");
        Component arbeiter2 = new Leaf("Bob");
        Component arbeiter3 = new Leaf("Thomas");
        list.add(arbeiter1);
        list.add(arbeiter2);
        list.add(arbeiter3);

        leiter2.add(arbeiter1);
        leiter2.add(arbeiter2);
        leiter3.add(arbeiter3);

        for (Component component : list) component.operation();
    }
}
