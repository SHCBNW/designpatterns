package composite_pattern;

public class Leaf extends Component {

    private String name;

    Leaf(String name) {
        setName(name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void operation() {
        System.out.println("Ich bin ein Leaf und heiße " + getName() + ".");
    }
}
