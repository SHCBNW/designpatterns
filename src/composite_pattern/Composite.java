package composite_pattern;

import java.util.ArrayList;
import java.util.List;

public class Composite extends Component {

    String name;
    private List<Component> children = new ArrayList<Component>();

    public Composite(String name) {
        setName(name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void operation() {
        System.out.println("Ich bin ein Composite und heiße " + getName() + ".\nMeine Handlanger sind:");
        for (Component child : children) {
            System.out.println("- " + child.getName());
        }
    }

    @Override
    public void add(Component comp) {
        children.add(comp);
    }

    @Override
    public void remove(Component comp) {
        children.remove(comp);
    }

    @Override
    public Component getChild(int index) {
        return children.get(index);
    }
}
